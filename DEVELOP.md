# Development

## Start web server locally

`docker-compose up`
  - At startup, runs `yarn` which installs packages from `package.json`, if
    they do not already exist at `node_modules/`
  - The user ID and group ID defaults to the value found inside the `.env` file.
    The purpose of this setting is to have all files written by the container
    into the bind mount (for example, `node_modules` and `yarn.lock`) to be
    owned by the current user instead of root
  - [`gatsby develop`](https://www.gatsbyjs.com/docs/reference/gatsby-cli/#develop)
    is run and the development server exposes http://localhost:8000


## Run a script

`docker-compose exec web yarn run reformat`
  - Replace `reformat` with one of the script names from `package.json` for
    other actions
  - Add the suffix `:about` to the script name for help on that command
    (for example, `reformat:about`)

### Make sure build is ready for production

`docker-compose exec web yarn run check:build`
