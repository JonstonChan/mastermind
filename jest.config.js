const config = {
  collectCoverageFrom: ["src/components/**/*.{js,jsx,ts,tsx}", "!**/*.d.*"],
  moduleNameMapper: {
    "\\.(s?css)$": "<rootDir>/__mocks__/styleMock.js",
  },
  reporters: ["default", "jest-junit"],
  setupFilesAfterEnv: ["<rootDir>/jest-setup.js"],
  testEnvironment: "jest-environment-jsdom",
}

module.exports = config
