module.exports = {
  pathPrefix: process.env.PATH_PREFIX || '/',
  plugins: [
    'gatsby-plugin-typescript',
    {
      resolve: 'gatsby-plugin-ts-checker',
      options: {
        typescript: {
          memoryLimit: 4096,
          mode: 'write-tsbuildinfo',
        },
      },
    },
  ],
}
