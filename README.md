# Mastermind

## Introduction

[Mastermind][Wikipedia: Mastermind] is a code-breaking game. The objective
is for the codebreaker to guess the codemaker's code in as few guesses as
possible.

## Gameplay

The codemaker's role is played by the computer in this version.

### Initial setup

At the beginning of the game, the codemaker generates a new code which is a
[permutation with repetition][]. The objects in this version are coloured pegs
and the code has a length of four pegs.

### Codebreaker

After the codemaker has completed initial setup, the codebreaker attempts to
guess the correct code made by the codemaker in as few guesses as possible.

### Codebreaker feedback

After each guess from the codebreaker, the codemaker provides the codebreaker
information regarding the guess:
- number of correctly-coloured pegs
- number of correctly-coloured pegs with the correct position

"correctly-coloured peg" in this context means a peg in the guess that has the
same colour as in the code. The number is the minimum of this coloured peg in
the code and guess. For example, if the code is "red red blue black" and the
guess is "red blue green green", then the codebreaker is told:
- two correctly-coloured pegs (because red and blue are in the code and guess)
- one correctly-coloured peg with the correct position (because only the red in
the guess has the same colour and position as the code)

### End of game

The game ends once the codebreaker has guessed the correct code or has reached
the maximum number of guesses allowed.


[permutation with repetition]: <https://brilliant.org/wiki/permutations-with-repetition/>
[Wikipedia: Mastermind]: <https://en.wikipedia.org/wiki/Mastermind_(board_game)>
