import React from "react"
import Chance from "chance"

import "./index.css"

import Mastermind, { IMastermindProps } from "../components/Mastermind"

const index = () => {
  const mastermindProps: IMastermindProps = {
    maxNumGuesses: 10,
    numChoices: 6,
    rng: new Chance(),
  }

  return (
    <main>
      <Mastermind {...mastermindProps} />
    </main>
  )
}

export default index
