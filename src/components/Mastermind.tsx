import React, { useState } from "react"
import { Button, Modal } from "semantic-ui-react"

import ControlPanel, { IControlPanelProps } from "./ControlPanel"
import DecoderBoard, { IDecoderBoardProps } from "./Decoder/Board"
import { IMastermindProps } from "./IMastermind"

import "./Mastermind.css"

const Mastermind = (props: IMastermindProps) => {
  const { maxNumGuesses, numChoices, rng } = props

  const codeLength = 4

  const generateCode = (length: number) =>
    Array.from({ length }, () => rng.integer({ min: 0, max: numChoices - 1 }))

  const [activeColorIndex, setActiveColorIndex] = useState<number>(0)
  const [code, setCode] = useState<number[]>(generateCode(codeLength))
  const [gameNumber, setGameNumber] = useState<number>(1)
  const [endGameModalOpen, setEndGameModalOpen] = useState<boolean>(false)
  const [isSuccessfulCompletion, setIsSuccessfulCompletion] =
    useState<boolean>(false)

  const colorPaletteHues = Array(numChoices)
    .fill(undefined)
    .map((_, index) => Math.floor(index * (360 / numChoices)))

  const resetBoard = () => {
    setActiveColorIndex(0)
    setCode(generateCode(codeLength))
    setGameNumber(gameNumber + 1)
  }

  const onGameEnd = (isSuccessfulCompletion: boolean) => {
    setIsSuccessfulCompletion(isSuccessfulCompletion)
    setEndGameModalOpen(true)
  }

  const endGameModal = (
    <Modal open={endGameModalOpen}>
      <Modal.Header>
        Game complete{isSuccessfulCompletion ? " (success)" : ""}
      </Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <p data-testid="end-of-game-description">
            This Mastermind round has been complete. You may start a new game
            with the restart icon under the color picker.
          </p>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          color="blue"
          content="Okay"
          onClick={() => setEndGameModalOpen(false)}
        />
      </Modal.Actions>
    </Modal>
  )

  const decoderBoardProps: IDecoderBoardProps = {
    activeColorIndex,
    code,
    colorPaletteHues,
    maxNumGuesses,
    onGameEnd,
  }

  const controlPanelProps: IControlPanelProps = {
    activeColorIndex,
    colorPaletteHues,
    numChoices,
    onSelectColor: (index: number) => setActiveColorIndex(index),
    resetBoard,
  }

  return (
    <div className="mastermind-container">
      {endGameModal}
      <div>
        <DecoderBoard key={gameNumber} {...decoderBoardProps} />
      </div>
      <div>
        <ControlPanel {...controlPanelProps} />
      </div>
    </div>
  )
}

export default Mastermind
export type { IMastermindProps }
