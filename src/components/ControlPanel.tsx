import React, { useState } from "react"
import { Button, Icon, Modal } from "semantic-ui-react"

import ColorPicker, { IColorPickerProps } from "./ColorPicker"
import { IControlPanelProps } from "./IControlPanel"

import "./ControlPanel.css"

const ControlPanel = (props: IControlPanelProps) => {
  const {
    activeColorIndex,
    colorPaletteHues,
    numChoices,
    onSelectColor,
    resetBoard,
  } = props

  const [confirmResetGameModalOpen, setConfirmResetGameModalOpen] =
    useState<boolean>(false)

  const colorPickerProps: IColorPickerProps = {
    activeColorIndex,
    colorPaletteHues,
    numChoices,
    onSelectColor,
  }

  return (
    <div className="control-panel">
      <ColorPicker {...colorPickerProps} />

      <Modal
        onClose={() => setConfirmResetGameModalOpen(false)}
        onOpen={() => setConfirmResetGameModalOpen(true)}
        open={confirmResetGameModalOpen}
        trigger={
          <div className="reset-icon-container">
            <Icon
              data-testid="control-panel-reset-game"
              name="refresh"
              size="big"
            />
          </div>
        }
      >
        <Modal.Header>Reset game</Modal.Header>
        <Modal.Content>Are you sure you want to reset the game?</Modal.Content>
        <Modal.Actions>
          <Button
            data-testid="control-panel-reset-game-cancel"
            content="Cancel"
            onClick={() => setConfirmResetGameModalOpen(false)}
          />
          <Button
            data-testid="control-panel-reset-game-confirm"
            positive
            content="Reset game"
            onClick={() => {
              setConfirmResetGameModalOpen(false)
              resetBoard()
            }}
          />
        </Modal.Actions>
      </Modal>
    </div>
  )
}

export default ControlPanel
export type { IControlPanelProps }
