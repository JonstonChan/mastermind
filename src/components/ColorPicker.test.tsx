import React from "react"
import { render, screen } from "@testing-library/react"

import ColorPicker, { IColorPickerProps } from "./ColorPicker"

describe("ColorPicker", () => {
  const colorPickerPropsBase: IColorPickerProps = {
    activeColorIndex: 0,
    colorPaletteHues: [0, 30, 60, 90, 120],
    numChoices: 5,
    onSelectColor: jest.fn(),
  }

  it("should have active color selected", () => {
    const { rerender } = render(<ColorPicker {...colorPickerPropsBase} />)
    expect(screen.getByTestId("color-picker").firstChild).toHaveClass(
      "selected"
    )
    expect(screen.getByTestId("color-picker").lastChild).not.toHaveClass(
      "selected"
    )

    const colorPickerPropsUpdated: IColorPickerProps = {
      ...colorPickerPropsBase,
      activeColorIndex: 4,
      numChoices: 5,
    }

    rerender(<ColorPicker {...colorPickerPropsUpdated} />)
    expect(screen.getByTestId("color-picker").firstChild).not.toHaveClass(
      "selected"
    )
    expect(screen.getByTestId("color-picker").lastChild).toHaveClass("selected")
  })
})
