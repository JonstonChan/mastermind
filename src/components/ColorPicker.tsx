import React from "react"

import HslColorBlock, { IHslColorBlockProps } from "./HslColorBlock"
import { IColorPickerProps } from "./IColorPicker"

import "./ColorPicker.css"

const ColorPicker = (props: IColorPickerProps) => {
  const { activeColorIndex } = props

  const choices = Array(props.numChoices)
    .fill(undefined)
    .map((_, index) => {
      const size = 40
      const isSelected = index === activeColorIndex

      const hslColorBlockProps: IHslColorBlockProps = {
        size,
        hue: props.colorPaletteHues[index],
        saturation: 100,
        lightness: isSelected ? "very high" : "medium",
      }

      return (
        <div
          key={index}
          data-testid="color-picker-choice"
          className={`choice ${isSelected ? "selected" : ""}`}
          onClick={() => props.onSelectColor(index)}
          style={{
            height: size,
            width: size,
          }}
        >
          <HslColorBlock {...hslColorBlockProps} />
        </div>
      )
    })

  return (
    <div className="choices-container" data-testid="color-picker">
      {choices}
    </div>
  )
}

export default ColorPicker
export type { IColorPickerProps }
