export interface IColorBlockProps {
  color: string
  size: number
}
