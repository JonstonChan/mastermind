import React from "react"
import ColorBlock, { IColorBlockProps } from "./ColorBlock"

import {
  IHslColorBlockProps,
  Hsl,
  Hue,
  Saturation,
  Lightness,
} from "./IHslColorBlock"

const HslColorBlock = (props: IHslColorBlockProps) => {
  const { hue, saturation, lightness } = props

  let hueVal: number
  let lightnessVal: number

  const lightnessMapping = {
    "very low": 95,
    "low": 85,
    "medium": 80,
    "high": 70,
    "very high": 50,
    "white": 100,
  }

  const hueMapping = {
    green: 120,
    red: 0,
    yellow: 60,
  }

  if (typeof lightness === "number") {
    lightnessVal = lightness
  } else {
    lightnessVal = lightnessMapping[lightness]
  }

  if (typeof hue === "number") {
    hueVal = hue
  } else {
    hueVal = hueMapping[hue]
  }

  const color = `hsl(${hueVal}, ${saturation}%, ${lightnessVal}%)`

  const colorBlockProps: IColorBlockProps = {
    color: color,
    size: props.size,
  }

  return <ColorBlock {...colorBlockProps} />
}

export default HslColorBlock
export type { IHslColorBlockProps, Hsl, Hue, Saturation, Lightness }
