export interface IHslColorBlockProps {
  size: number
  hue: Hue
  saturation: Saturation
  lightness: Lightness
}

export interface Hsl {
  hue: Hue
  saturation: Saturation
  lightness: Lightness
}

export type Hue =
  | number // integer between 0 (inclusive) and 360 (inclusive)
  | "green"
  | "red"
  | "yellow"

export type Saturation = number // integer between 0 (inclusive) and 100 (inclusive)
export type Lightness =
  | number // integer between 0 (inclusive) and 100 (inclusive)
  | "very low"
  | "low"
  | "medium"
  | "high"
  | "very high"
  | "white"
