import React from "react"

import { IColorBlockProps } from "./IColorBlock"

const ColorBlock = (props: IColorBlockProps) => {
  const style: React.CSSProperties = {
    backgroundColor: props.color,
    display: "flex",
    height: props.size,
    width: props.size,
  }

  return <div style={style} />
}

export default ColorBlock
export type { IColorBlockProps }
