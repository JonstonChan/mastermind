export interface IMastermindProps {
  maxNumGuesses: number
  numChoices: number
  rng: Chance.Chance
}
