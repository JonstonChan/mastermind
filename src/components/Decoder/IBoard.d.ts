export interface IDecoderBoardProps {
  activeColorIndex: number
  code: number[]
  colorPaletteHues: number[]
  maxNumGuesses: number
  onGameEnd: (isSuccessfulCompletion: boolean) => void
}
