import React from "react"
import { fireEvent, render, screen } from "@testing-library/react"

import DecoderBoard, { exportedForTesting, IDecoderBoardProps } from "./Board"
import { IHint } from "./IHint"

describe("Board", () => {
  describe("getHint", () => {
    const { getHint } = exportedForTesting

    const getHintCases: [string, number[], number[], number[]][] = [
      [
        "all correct color and positions",
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [4, 4, 0],
      ],
      [
        "all correct color and all wrong positions",
        [0, 0, 1, 1],
        [1, 1, 0, 0],
        [4, 0, 0],
      ],
      [
        "all correct color and some wrong positions",
        [3, 0, 4, 0],
        [3, 4, 0, 0],
        [4, 2, 0],
      ],
      [
        "code with more of correct color than guess",
        [0, 0, 1, 1],
        [0, 0, 0, 0],
        [2, 2, 2],
      ],
      [
        "guess with more of correct color than code",
        [0, 0, 0, 0],
        [0, 0, 1, 1],
        [2, 2, 2],
      ],
      ["guess with large value", [0, 1000, 0, 0], [0, 0, 1, 1], [2, 1, 2]],
      ["code with large value", [0, 0, 0, 0], [0, 1000, 1, 1], [1, 1, 3]],
      [
        "some correct color and positions",
        [0, 1, 1, 2],
        [1, 0, 1, 0],
        [3, 1, 1],
      ],
      ["all incorrect colors", [0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 4]],
    ]

    it.each(getHintCases)(
      "getHint (%s) of code %p and guess %p has hint %p",
      (_: string, code: number[], guess: number[], hint: number[]) => {
        const expectedHint: IHint = {
          numCorrectColor: hint[0],
          numCorrectColorAndPosition: hint[1],
          numNoInformation: hint[2],
        }
        expect(getHint(code, guess)).toStrictEqual(expectedHint)
      }
    )
  })

  describe("render", () => {
    const decoderBoardPropsBase: IDecoderBoardProps = {
      activeColorIndex: 0,
      code: [0, 0, 0, 0],
      colorPaletteHues: [0, 30, 60, 90, 120],
      maxNumGuesses: 2,
      onGameEnd: jest.fn(),
    }

    it("it calls game end function when game ends successfully", () => {
      const onGameEndFunctionMock = jest.fn()
      const decoderBoardPropsUpdated: IDecoderBoardProps = {
        ...decoderBoardPropsBase,
        onGameEnd: onGameEndFunctionMock,
      }

      render(<DecoderBoard {...decoderBoardPropsUpdated} />)

      const codeLength = decoderBoardPropsUpdated.code.length

      for (let i = 0; i < codeLength; ++i) {
        const guessElement = screen.getByTestId(
          `active-decoder-row-guess-element-${i}`
        )
        fireEvent.click(guessElement)
      }

      const submitRowButton = screen.getByTestId(
        "active-decoder-row-submit-button"
      )
      fireEvent.click(submitRowButton)

      expect(onGameEndFunctionMock.mock.calls.length).toBe(1)

      // Expect first function call's first argument
      expect(onGameEndFunctionMock.mock.calls[0][0]).toBe(true)
    })

    it("it calls game end function when game ends unsuccessfully", () => {
      const onGameEndFunctionMock = jest.fn()
      const decoderBoardPropsUpdated: IDecoderBoardProps = {
        ...decoderBoardPropsBase,
        activeColorIndex: 0,
        code: [1, 1, 1, 1],
        maxNumGuesses: 1,
        onGameEnd: onGameEndFunctionMock,
      }

      render(<DecoderBoard {...decoderBoardPropsUpdated} />)

      const codeLength = decoderBoardPropsUpdated.code.length

      for (let i = 0; i < codeLength; ++i) {
        const guessElement = screen.getByTestId(
          `active-decoder-row-guess-element-${i}`
        )
        fireEvent.click(guessElement)
      }

      const submitRowButton = screen.getByTestId(
        "active-decoder-row-submit-button"
      )
      fireEvent.click(submitRowButton)

      expect(onGameEndFunctionMock.mock.calls.length).toBe(1)

      // Expect first function call's first argument
      expect(onGameEndFunctionMock.mock.calls[0][0]).toBe(false)
    })
  })
})
