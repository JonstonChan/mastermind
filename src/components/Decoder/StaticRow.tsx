import React from "react"
import { Popup } from "semantic-ui-react"

import HslColorBlock, { IHslColorBlockProps } from "../HslColorBlock"
import { HintType } from "./IHint"
import { IStaticRowProps } from "./IStaticRow"

import "./StaticRow.css"

const StaticRow = (props: IStaticRowProps) => {
  const { guessHues, isGuessed } = props

  const isNotGuessedSaturation = 0
  const isNotGuessedLightness = 100

  const guessColorBlocks = guessHues.map((hue, index) => {
    const size = 50

    const hslColorBlockProps: IHslColorBlockProps = {
      hue,
      size,
      saturation: isGuessed ? 100 : isNotGuessedSaturation,
      lightness: isGuessed ? "low" : isNotGuessedLightness,
    }

    return (
      <div
        key={index}
        className="guess"
        style={{
          height: size,
          width: size,
        }}
      >
        <HslColorBlock {...hslColorBlockProps} />
      </div>
    )
  })

  const { numCorrectColor, numCorrectColorAndPosition, numNoInformation } =
    props.hint

  const hintColorBlocks = Array(numCorrectColor + numNoInformation)
    .fill(undefined)
    .map((_, index) => {
      const size = 22

      const hintType: HintType =
        index < numCorrectColorAndPosition
          ? "color-and-position"
          : index < numCorrectColor
          ? "color-only"
          : "no-information"

      const hslColorBlockProps: IHslColorBlockProps = {
        hue:
          hintType === "color-and-position"
            ? "green"
            : hintType === "color-only"
            ? "yellow"
            : "red",
        size,
        saturation: isGuessed ? 100 : isNotGuessedSaturation,
        lightness: isGuessed ? "very high" : isNotGuessedLightness,
      }

      return (
        <div
          key={index}
          data-testid={`hint-block-${hintType}`}
          className="hint"
          style={{
            height: size,
            width: size / 2,
          }}
        >
          <HslColorBlock {...hslColorBlockProps} />
        </div>
      )
    })

  const hintPopupMessage = isGuessed ? (
    <Popup.Content>
      Correct color and position: <b>{numCorrectColorAndPosition}</b>
      <br />
      Correct color, but wrong position:{" "}
      <b>{numCorrectColor - numCorrectColorAndPosition}</b>
      <br />
      Incorrect color: <b>{numNoInformation}</b>
      <br />
    </Popup.Content>
  ) : (
    <Popup.Content>
      Once you make a guess for this row,
      <br />
      it will be populated with hints
    </Popup.Content>
  )

  return (
    <div className="decoder-row">
      <div className="guesses-container">{guessColorBlocks}</div>

      <Popup
        wide
        trigger={<div className="hints-container">{hintColorBlocks}</div>}
      >
        {hintPopupMessage}
      </Popup>
    </div>
  )
}

export default StaticRow
export type { IStaticRowProps }
