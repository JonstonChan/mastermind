export type IHint = {
  numCorrectColor: number
  numCorrectColorAndPosition: number
  numNoInformation: number
}

export type HintType = "color-and-position" | "color-only" | "no-information"
