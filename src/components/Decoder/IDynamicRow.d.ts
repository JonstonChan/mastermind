export interface IDynamicRowProps {
  activeColorIndex: number
  codeLength: number
  colorPaletteHues: number[]
  onSubmit: (codeGuess: number[]) => void
  previousGuesses: number[][]
}
