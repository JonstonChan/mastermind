import React, { useEffect, useState } from "react"

import { IDecoderBoardProps } from "./IBoard"
import { IHint } from "./IHint"
import DynamicRow, { IDynamicRowProps } from "./DynamicRow"
import StaticRow, { IStaticRowProps } from "./StaticRow"

import "./Board.css"

const DecoderBoard = (props: IDecoderBoardProps) => {
  const [guesses, setGuesses] = useState<number[][]>([])
  const [isGameEnded, setIsGameEnded] = useState<boolean>(false)

  const { activeColorIndex, code, colorPaletteHues, maxNumGuesses, onGameEnd } =
    props

  useEffect(() => {
    setGuesses([])
  }, [code])

  useEffect(() => {
    if (guesses.length > 0) {
      const mostRecentGuess = guesses.at(-1)

      if (typeof mostRecentGuess !== "undefined") {
        if (
          mostRecentGuess.every((element, index) => code[index] === element)
        ) {
          setIsGameEnded(true)
          onGameEnd(true)
          return
        }
      }
    }

    if (guesses.length === maxNumGuesses) {
      setIsGameEnded(true)
      onGameEnd(false)
    }
  }, [guesses, maxNumGuesses])

  const staticRowsGuessed = guesses.map((guess, index) => {
    const guessHues = guess.map((codeElement) => colorPaletteHues[codeElement])

    const hint: IHint = getHint(code, guess)

    const staticRowProps: IStaticRowProps = {
      guessHues,
      hint,
      isGuessed: true,
    }

    return <StaticRow key={index} {...staticRowProps} />
  })

  const onSubmit = (codeGuess: number[]) => {
    setGuesses((prevGuess) => [...prevGuess, codeGuess])
  }

  const activeRowProps: IDynamicRowProps = {
    activeColorIndex,
    codeLength: code.length,
    colorPaletteHues,
    onSubmit,
    previousGuesses: guesses,
  }
  const activeRow = isGameEnded ? null : <DynamicRow {...activeRowProps} />

  const numRemainingGuesses = maxNumGuesses - guesses.length
  // Subtract one to account for the active, in progress guess if the game hasn't ended yet
  const staticRowsUnguessed = Array(
    Math.max(numRemainingGuesses - (isGameEnded ? 0 : 1), 0)
  )
    .fill(undefined)
    .map((_, index) => {
      const codeSize = code.length

      // Hue value does not matter, as no information should be revealed in unguessed rows
      const hues = Array(codeSize).fill(0)

      const hint: IHint = {
        numCorrectColor: 0,
        numCorrectColorAndPosition: 0,
        numNoInformation: codeSize,
      }

      const staticRowProps: IStaticRowProps = {
        guessHues: hues,
        hint,
        isGuessed: false,
      }

      return <StaticRow key={index} {...staticRowProps} />
    })

  return (
    <div className="decoder-board">
      <div>{staticRowsGuessed}</div>
      <div className="active-decoder-row-container">
        {numRemainingGuesses ? activeRow : null}
      </div>
      <div>{staticRowsUnguessed}</div>
    </div>
  )
}

const getHint = (code: number[], guess: number[]) => {
  const getNumCorrectColor = () => {
    const numPossibleCodeElements = Math.max(...code, ...guess) + 1

    const guessColorCounts = Array(numPossibleCodeElements).fill(0)
    guess.forEach((colorIndex) => ++guessColorCounts[colorIndex])

    const codeColorCounts = Array(numPossibleCodeElements).fill(0)
    code.forEach((colorIndex) => ++codeColorCounts[colorIndex])

    const numCorrectColor = Array(numPossibleCodeElements)
      .fill(undefined)
      .reduce(
        (previousVal, _, index) =>
          previousVal +
          Math.min(guessColorCounts[index], codeColorCounts[index]),
        0
      )

    return numCorrectColor
  }

  const getNumCorrectColorAndPosition = () =>
    guess.reduce(
      (previousVal, _, index) =>
        previousVal + (guess[index] === code[index] ? 1 : 0),
      0
    )

  const numCorrectColor = getNumCorrectColor()
  const numCorrectColorAndPosition = getNumCorrectColorAndPosition()
  const numNoInformation = code.length - numCorrectColor

  const hint: IHint = {
    numCorrectColor,
    numCorrectColorAndPosition,
    numNoInformation,
  }

  return hint
}

export const exportedForTesting = {
  getHint,
}

export default DecoderBoard
export type { IDecoderBoardProps }
