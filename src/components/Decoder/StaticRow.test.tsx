import React from "react"
import { render, screen } from "@testing-library/react"

import { IHint } from "./IHint"
import StaticRow, { IStaticRowProps } from "./StaticRow"

describe("StaticRow", () => {
  const staticRowPropsBase: Omit<IStaticRowProps, "hint" | "isGuessed"> = {
    guessHues: [0, 30, 60, 90, 120, 150],
  }

  const hintBlockTypeColorAndPositionId = "hint-block-color-and-position"
  const hintBlockTypeColorOnlyId = "hint-block-color-only"
  const hintBlockTypeNoInformationId = "hint-block-no-information"

  describe("isGuessed", () => {
    const hint: IHint = {
      numCorrectColor: 3,
      numCorrectColorAndPosition: 2,
      numNoInformation: 1,
    }

    const staticRowProps: IStaticRowProps = {
      ...staticRowPropsBase,
      hint,
      isGuessed: true,
    }

    it("should have correct number of hint tiles", () => {
      render(<StaticRow {...staticRowProps} />)
      expect(
        screen.getAllByTestId(hintBlockTypeColorAndPositionId).length
      ).toBe(2)
      expect(screen.getAllByTestId(hintBlockTypeColorOnlyId).length).toBe(1)
      expect(screen.getAllByTestId(hintBlockTypeNoInformationId).length).toBe(1)
    })
  })

  describe("isNotGuessed", () => {
    const hint: IHint = {
      numCorrectColor: 0,
      numCorrectColorAndPosition: 0,
      numNoInformation: 4,
    }

    const staticRowProps: IStaticRowProps = {
      ...staticRowPropsBase,
      hint,
      isGuessed: false,
    }

    it("should not reveal information in hint tiles", () => {
      render(<StaticRow {...staticRowProps} />)
      expect(
        screen.queryAllByTestId(hintBlockTypeColorAndPositionId).length
      ).toBe(0)
      expect(screen.queryAllByTestId(hintBlockTypeColorOnlyId).length).toBe(0)
      expect(screen.getAllByTestId(hintBlockTypeNoInformationId).length).toBe(4)
    })
  })
})
