import { IHint } from "./IHint"

export interface IStaticRowProps {
  guessHues: number[]
  hint: IHint
  isGuessed: boolean
}
