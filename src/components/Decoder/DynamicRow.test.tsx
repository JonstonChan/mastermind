import React from "react"
import { fireEvent, render, screen } from "@testing-library/react"

import DynamicRow, { IDynamicRowProps } from "./DynamicRow"

describe("DynamicRow", () => {
  const dynamicRowPropsBase: IDynamicRowProps = {
    activeColorIndex: 0,
    codeLength: 4,
    colorPaletteHues: [0, 30, 60, 90, 120, 150],
    onSubmit: jest.fn(),
    previousGuesses: [],
  }

  const activeDecoderRowSubmitId = "active-decoder-row-submit-button"

  it("should have disabled submit initially", () => {
    render(<DynamicRow {...dynamicRowPropsBase} />)
    expect(screen.getByTestId(activeDecoderRowSubmitId)).toBeDisabled()
  })

  it("should have submit enabled after a complete guess is chosen", () => {
    render(<DynamicRow {...dynamicRowPropsBase} />)

    for (let i = 0; i < dynamicRowPropsBase.codeLength; ++i) {
      expect(screen.getByTestId(activeDecoderRowSubmitId)).toBeDisabled()

      const guessElement = screen.getByTestId(
        `active-decoder-row-guess-element-${i}`
      )
      fireEvent.click(guessElement)
    }

    expect(screen.getByTestId(activeDecoderRowSubmitId)).toBeEnabled()
  })

  it("should have submit disabled if current guess is same as any previous guess", () => {
    const dynamicRowProps: IDynamicRowProps = {
      ...dynamicRowPropsBase,
      activeColorIndex: 1,
      previousGuesses: [[1, 1, 1, 1]],
    }

    const { rerender } = render(<DynamicRow {...dynamicRowProps} />)

    for (let i = 0; i < dynamicRowProps.codeLength; ++i) {
      const guessElement = screen.getByTestId(
        `active-decoder-row-guess-element-${i}`
      )
      fireEvent.click(guessElement)
    }

    expect(screen.getByTestId(activeDecoderRowSubmitId)).toBeDisabled()

    const dynamicRowPropsUpdated: IDynamicRowProps = {
      ...dynamicRowProps,
      activeColorIndex: 2,
    }

    rerender(<DynamicRow {...dynamicRowPropsUpdated} />)
    expect(screen.getByTestId(activeDecoderRowSubmitId)).toBeDisabled()

    const guessElement = screen.getByTestId(
      "active-decoder-row-guess-element-0"
    )
    fireEvent.click(guessElement)

    expect(screen.getByTestId(activeDecoderRowSubmitId)).toBeEnabled()
  })
})
