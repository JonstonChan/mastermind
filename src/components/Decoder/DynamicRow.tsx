import React, { useEffect, useState } from "react"
import { Button, Popup } from "semantic-ui-react"

import HslColorBlock, { IHslColorBlockProps, Hsl } from "../HslColorBlock"
import { IDynamicRowProps } from "./IDynamicRow"

import "./DynamicRow.css"

const DynamicRow = (props: IDynamicRowProps) => {
  const { activeColorIndex, codeLength, colorPaletteHues, previousGuesses } =
    props

  const [guess, setGuess] = useState<(number | undefined)[]>(
    Array(codeLength).fill(undefined)
  )

  const [isCompleteGuess, setIsCompleteGuess] = useState<boolean>(false)
  const [isAlreadyGuessed, setIsAlreadyGuessed] = useState<boolean>(false)

  const defaultSubmitButtonMessage =
    "You must make a guess for each element of the code before submitting"
  const [enableSubmitButton, setEnableSubmitButton] = useState<boolean>(false)
  const [submitButtonMessage, setSubmitButtonMessage] = useState<string>(
    defaultSubmitButtonMessage
  )

  const updateGuess = (guessIndex: number, colorIndex: number) => {
    const updatedGuess = [...guess]
    updatedGuess[guessIndex] = colorIndex
    setGuess(updatedGuess)
  }

  useEffect(() => {
    const isCompleteGuess = guess.every(
      (element) => typeof element !== "undefined"
    )

    setIsCompleteGuess(isCompleteGuess)
  }, [guess, previousGuesses])

  useEffect(() => {
    if (isCompleteGuess) {
      const currentGuess = guess as number[]

      const isSameAsCurrentGuess = (guess: number[]) =>
        guess.every((value, index) => value === currentGuess[index])

      const isAlreadyGuessed = previousGuesses.some((prevGuess) =>
        isSameAsCurrentGuess(prevGuess)
      )

      setIsAlreadyGuessed(isAlreadyGuessed)
    }
  }, [guess, isCompleteGuess, previousGuesses])

  useEffect(() => {
    if (isCompleteGuess) {
      if (isAlreadyGuessed) {
        setSubmitButtonMessage(
          "You have already made this guess before. Make a change to your guess before submitting"
        )
      } else {
        setSubmitButtonMessage(
          "Submit guess and get information about the correct code"
        )
        setEnableSubmitButton(true)
        return
      }
    } else {
      setSubmitButtonMessage(defaultSubmitButtonMessage)
    }

    setEnableSubmitButton(false)
  }, [isAlreadyGuessed, isCompleteGuess, previousGuesses])

  const isNotGuessedSaturation = 0
  const isNotGuessedLightness = 100

  const guessColorBlocks = guess.map((colorIndex, index) => {
    const size = 50

    let hsl: Hsl

    // Current TypeScript checker does not allow extracting condition below into a variable
    // See https://stackoverflow.com/q/67787105
    if (typeof colorIndex !== "undefined") {
      hsl = {
        hue: colorPaletteHues[colorIndex],
        saturation: 100,
        lightness: "very high",
      }
    } else {
      hsl = {
        hue: 0,
        saturation: isNotGuessedSaturation,
        lightness: isNotGuessedLightness,
      }
    }

    const hslColorBlockProps: IHslColorBlockProps = {
      size,
      ...hsl,
    }

    return (
      <div
        data-testid={`active-decoder-row-guess-element-${index}`}
        key={index}
        className="active guess"
        style={{
          height: size,
          width: size,
        }}
        onClick={() => updateGuess(index, activeColorIndex)}
      >
        <HslColorBlock {...hslColorBlockProps} />
      </div>
    )
  })

  return (
    <div className="active decoder-row">
      <div className="active guesses-container">{guessColorBlocks}</div>
      <Popup
        wide
        content={submitButtonMessage}
        trigger={
          // div container is a workaround for popup not showing when button is disabled
          // see https://github.com/Semantic-Org/Semantic-UI-React/issues/1413
          <div className="active-decoder-row-button-container">
            <Button
              data-testid="active-decoder-row-submit-button"
              fluid
              disabled={!enableSubmitButton}
              content="Submit"
              onClick={() => {
                if (enableSubmitButton) {
                  const codeGuess = guess as number[]
                  props.onSubmit(codeGuess)
                }
              }}
            />
          </div>
        }
      />
    </div>
  )
}

export default DynamicRow
export type { IDynamicRowProps }
