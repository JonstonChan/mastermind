import React from "react"
import Chance from "chance"
import { fireEvent, render, screen } from "@testing-library/react"

import Mastermind, { IMastermindProps } from "./Mastermind"

describe("Mastermind", () => {
  const mastermindPropsBase: IMastermindProps = {
    maxNumGuesses: 10,
    numChoices: 6,
    rng: new Chance(+new Date("2022-01-01")),
  }

  const codeLength = 4

  it("should show message on game end", () => {
    render(<Mastermind {...mastermindPropsBase} />)

    const colorPickerChoices = screen.getAllByTestId("color-picker-choice")

    expect(screen.queryByTestId("end-of-game-description")).toBe(null)

    const guess = [1, 1, 3, 1]
    expect(guess.length).toBe(codeLength)

    for (let i = 0; i < guess.length; ++i) {
      fireEvent.click(colorPickerChoices[guess[i]])

      const guessElement = screen.getByTestId(
        `active-decoder-row-guess-element-${i}`
      )
      fireEvent.click(guessElement)
    }

    const submitRowButton = screen.getByTestId(
      "active-decoder-row-submit-button"
    )
    fireEvent.click(submitRowButton)

    expect(screen.getByTestId("end-of-game-description")).toBeVisible()
  })
})
