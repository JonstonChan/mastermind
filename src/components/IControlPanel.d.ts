export interface IControlPanelProps {
  activeColorIndex: number
  colorPaletteHues: number[]
  numChoices: number
  onSelectColor: (index: number) => void
  resetBoard: () => void
}
