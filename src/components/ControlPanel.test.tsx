import React from "react"
import { fireEvent, render, screen } from "@testing-library/react"

import ControlPanel, { IControlPanelProps } from "./ControlPanel"

describe("ColorPicker", () => {
  const controlPanelPropsBase: IControlPanelProps = {
    activeColorIndex: 0,
    colorPaletteHues: [0, 30, 60, 90, 120],
    numChoices: 6,
    onSelectColor: jest.fn(),
    resetBoard: jest.fn(),
  }

  it("should call reset function when user requests a reset", () => {
    const resetFunctionMock = jest.fn()
    const controlPanelPropsUpdated: IControlPanelProps = {
      ...controlPanelPropsBase,
      resetBoard: resetFunctionMock,
    }

    render(<ControlPanel {...controlPanelPropsUpdated} />)

    const resetGameButton = screen.getByTestId("control-panel-reset-game")
    fireEvent.click(resetGameButton)

    const resetGameConfirmButton = screen.getByTestId(
      "control-panel-reset-game-confirm"
    )
    fireEvent.click(resetGameConfirmButton)

    expect(resetFunctionMock.mock.calls.length).toBe(1)
  })
})
