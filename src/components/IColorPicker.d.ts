export interface IColorPickerProps {
  activeColorIndex: number
  colorPaletteHues: number[]
  numChoices: number
  onSelectColor: (index: number) => void // index is zero-indexed index of selected choice
}
